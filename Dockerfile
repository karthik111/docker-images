FROM ubuntu:18.04
RUN apt update -y && apt install wget -y
RUN wget https://get.helm.sh/helm-v3.10.0-linux-amd64.tar.gz
RUN tar xvf helm-v3.10.0-linux-amd64.tar.gz
RUN mv linux-amd64/helm /usr/local/bin

